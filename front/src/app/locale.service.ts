import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeEn from '@angular/common/locales/en';
import { TranslocoService } from '@ngneat/transloco';
import * as dayjs from 'dayjs';
import 'dayjs/locale/fr';

const locales = {
	en: localeEn,
	fr: localeFr,
}

@Injectable()
export class LocaleService {
	constructor(
		@Inject(LOCALE_ID) private locale: string,
		private translocoService: TranslocoService
	) {}

	setLocale(fullLocale: string): void {
		const locale: string = fullLocale.substring(0,2);
		this.locale = locale;
		dayjs.locale(locale);
		this.translocoService.setActiveLang(locale);

		registerLocaleData(locales.en);
		registerLocaleData(locales.fr);
	}
}