import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { combineLatest, concat, interval, timer } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

@Component({
  selector: 'app-util',
  templateUrl: './util.component.html',
  styleUrls: ['./util.component.scss']
})
export class UtilComponent {
  url = 'http://localhost:3000';
  message = '';
  submessage = '';

  constructor(private http: HttpClient) { }

  start(): void {
    this.message = 'Retrieving products';
    this.submessage = '';
    combineLatest([
      this.http.get<any[]>(`${this.url}/products`),
      this.http.get<any[]>(`${this.url}/fav`)
    ])
    .subscribe(([products, fav]) => {
      this.message = `Got products (${products.length})`;
      if (fav?.length) {
        this.message += `- Found ${fav.length} saved fruits and vegetables`;
      }

      timer(2500).subscribe(() => {
        this.message = 'Filtering products...';
        const remaining = products.filter(product => product.groupe === 'fruitsandvegetables')
          .filter(product => !fav.find(edible => edible.nom_francais === product.nom_francais));
        this.message = `Found ${remaining.length} fruits and vegetables to save`;

        timer(2500).subscribe(() => {
          let saved = 0;
          this.message = 'Saving fruits and vegetables';
          const temporization = interval(1000).pipe(take(fav.length));
          concat(temporization)
            .pipe(switchMap((index) => this.http.post(`${this.url}/fav`, remaining[index])))
            .subscribe(() => {
              this.submessage = `Saved ${remaining[saved].nom_francais} (${saved + 1} / ${remaining.length})`;
              saved++;
            },
            () => {
              this.submessage = `Failed to save ${remaining[saved].nom_francais}`;
            });
        },
        () => this.message = 'Error while saving fruit or vegetable');
      });
    },
    () => this.message = 'Error while retrieving products');
  }

}
