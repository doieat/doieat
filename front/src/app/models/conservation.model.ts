import { ConservationRange } from './conservation-range.type';

export interface IConservation {
	range: ConservationRange;
	description: string;
}