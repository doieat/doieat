export enum ENutriscore {
	excellent = 'A',
	good = 'B',
	average = 'C',
	poor = 'D',
	bad = 'E'
}