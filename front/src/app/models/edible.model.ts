import { getEdibleAvailability } from '../data';
import { IConservation } from './conservation.model'
import { EAvailabilityType } from './availability-type.enum';
import { ConservationRange } from './conservation-range.type';
import { EEdibleGroup } from './edible-group.enum';
import { ENutriscore } from './nutriscore.enum';

export interface IEdible {
	availability: EAvailabilityType[]; // describes an array of months
	rawConservation: IConservation;
	refrigeratedConservation: IConservation;

	description: string;
	name: string;
	detailedName: string;
	group: EEdibleGroup;
	nutriscore: ENutriscore;
	price: number;
	perishable: boolean;
	picture: string;

	// Agrybalyse dataset
	pef: number;
	dqr: number;
	byPlane: boolean;
	preparation: string;
	wrap: string;
	ciqualCode: number;
}

export class Edible implements IEdible {
	availability: EAvailabilityType[] = []; // describes an array of months
	rawConservation: IConservation = {
		range: {
			min: 0,
			max: 0,
		},
		description: '',
	};
	refrigeratedConservation: IConservation = {
		range: {
			min: 0,
			max: 0,
		},
		description: '',
	};
	description: string = '';
	name: string = '';
	detailedName: string = '';
	group: EEdibleGroup = 0;
	nutriscore: ENutriscore = ENutriscore.average;
	price: number =  0;
	perishable: boolean = false;
	picture: string = '';

	// Agrybalyse dataset
	pef: number = 0;
	dqr: number = 0;
	byPlane: boolean = false;
	preparation: string = '';
	wrap: string = '';
	ciqualCode: number = 0;

	constructor(edible?: Partial<IEdible>) {
		if (edible) {
			Object.assign(this, edible);
		}
	}
}

/**
 * Utilities
 */

export interface IRawEdible {
	nom_francais: string;
	LCI_name: string;
	ciqual_code: string;
	groupe: string;
	sous_groupe: string;
	saison: string;
	avion: boolean;
	materiau_emballage: string;
	Livraison: string;
	Preparation: string;
	DQR: IDQR;
	impact_environnemental: IEnvironmentalImpact;
}

interface IEnvironmentalImpact {
	"Score unique EF": IPEF;
}

class EnvironmentalImpact {
	"Score unique EF": IPEF;

	constructor(impact?: IEnvironmentalImpact) {
		if (impact) {
			impact["Score unique EF"] = new PEF();
			Object.assign(this, impact);
		}
	}
}

interface IPEF {
	"synthese": number;
	"unite": string;
	"etapes": IEnvironmentalSteps;
}

class PEF {
	"synthese": number;
	"unite": string;
	"etapes": IEnvironmentalSteps;

	constructor(pef?: IPEF) {
		if (pef) {
			pef.etapes = new EnvironmentalSteps();
			Object.assign(this, pef);
		}
	}	
}

interface IEnvironmentalSteps {
	"Agriculture": number;
	"Transformation": number;
	"Emballage": number;
	"Transport": number;
	"Supermarché et distribution": number;
	"Consommation": number;
}

class EnvironmentalSteps implements IEnvironmentalSteps {
	"Agriculture": number;
	"Transformation": number;
	"Emballage": number;
	"Transport": number;
	"Supermarché et distribution": number;
	"Consommation": number;

	constructor(steps?: IEnvironmentalSteps) {
		if (steps) {
			Object.assign(this, steps);
		}
	}
}

interface IDQR {
	overall: number;
	"P": number;
	"TiR": number;
	"GR": number;
	"TeR": number;
}

class DQR implements IDQR{
	overall: number = 0;
	"P": number = 0;
	"TiR": number = 0;
	"GR": number = 0;
	"TeR": number = 0;

	constructor(dqr?: IDQR) {
		if (dqr) {
			this.overall = +dqr.overall;
			this.P = +dqr.P;
			this.TiR = +dqr.TiR;
			this.GR = +dqr.GR;
			this.TeR = +dqr.TeR;
		}
	}
}

export class RawEdible implements IRawEdible {
	nom_francais: string = '';
	LCI_name: string = '';
	ciqual_code: string = '';
	groupe: string = '';
	sous_groupe: string = '';
	saison: string = '';
	avion: boolean = false;
	materiau_emballage: string = '';
	Livraison: string = '';
	Preparation: string = '';
	DQR: DQR = new DQR();
	impact_environnemental: IEnvironmentalImpact = new EnvironmentalImpact();

	constructor(edible: IRawEdible) {
		if (edible) {
			Object.assign(this, edible);
		}
	}

	parseEdible(): IEdible {
		const names = this.nom_francais.split(',');

		let edibleBase: Partial<IEdible> = {
			name: names[0],
			detailedName: names[1],
			preparation: this.Preparation,
			availability: getEdibleAvailability(this.nom_francais),
			byPlane: this.avion,
			ciqualCode: +this.ciqual_code,
			group: this.sous_groupe === 'légumes' ? EEdibleGroup.vegetable : EEdibleGroup.fruit,
			wrap: this.materiau_emballage,
			pef: this.impact_environnemental["Score unique EF"]?.synthese,
			dqr: this.DQR.overall,
		}

		return new Edible(edibleBase);
	}
}
