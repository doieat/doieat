export type ConservationRange = {
	min: number; // Duration in milliseconds
	max: number;
}