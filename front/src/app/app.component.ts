import { Component } from '@angular/core';
import { LocaleService } from './locale.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'DOIEAT';

  constructor(private localeService: LocaleService) {}
  
  ngOnInit(): void {
    this.localeService.setLocale('fr');
  }
}
