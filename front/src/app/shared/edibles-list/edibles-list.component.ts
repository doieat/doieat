import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IEdible } from '@models';

@Component({
  selector: 'app-edibles-list',
  templateUrl: './edibles-list.component.html',
  styleUrls: ['./edibles-list.component.scss']
})
export class EdiblesListComponent implements OnChanges {
  @Input() edibles!: IEdible[] | null;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.edibles && changes.edibles.currentValue) {
      console.log('List changes', this.edibles);
    }
  }
}
