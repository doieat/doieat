import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiblesListComponent } from './edibles-list.component';

describe('EdiblesListComponent', () => {
  let component: EdiblesListComponent;
  let fixture: ComponentFixture<EdiblesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdiblesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiblesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
