import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IEdible, IRawEdible, RawEdible } from '@models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class EdibleInformationService {
  url: string = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  getEdibleInformation(edibleClue: string): Observable<IEdible[]> {
    console.log('Get edible information', edibleClue);
    return this.http.get<IRawEdible[]>(`${this.url}/fav?q=${edibleClue}`)
      .pipe(map((edibles) =>  edibles.map(edible => new RawEdible(edible).parseEdible())));
  }

  getEdibleById(ciqualCode: number): Observable<IEdible | undefined> {
    return this.http.get<IRawEdible[]>(`${this.url}/fav?ciqual_code=${ciqualCode}`)
      .pipe(map((edibles) =>  edibles.map(edible => new RawEdible(edible).parseEdible()).pop()));
  }
}
