import { TestBed } from '@angular/core/testing';

import { EdibleInformationService } from './edible-information.service';

describe('EdibleInformationService', () => {
  let service: EdibleInformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EdibleInformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
