import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

import { SearchBoxComponent } from './search-box/search-box.component';
import { EdiblesListComponent } from './edibles-list/edibles-list.component';
import { EdiblesListEntryComponent } from './edibles-list-entry/edibles-list-entry.component';
import { EdibleInformationService } from './services';
import { EdibleFeaturedComponent } from './edible-featured/edible-featured.component';

@NgModule({
  declarations: [
    SearchBoxComponent,
    EdiblesListComponent,
    EdiblesListEntryComponent,
    EdibleFeaturedComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    ScrollingModule,
  ],
  exports: [
    SearchBoxComponent,
    EdiblesListComponent,
    EdiblesListEntryComponent,
    EdibleFeaturedComponent,
  ],
  providers: [
    EdibleInformationService,
  ]
})
export class SharedModule { }
