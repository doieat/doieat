import { Component, forwardRef, Input } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';

import { AControlAccessor } from '../control-accessor.abstract';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss'],
  providers: [{
		provide: NG_VALUE_ACCESSOR,
		useExisting: forwardRef(() => SearchBoxComponent),
		multi: true,
	}],
})
export class SearchBoxComponent extends AControlAccessor<string> {
  @Input() options: string[] = [];
  input = new FormControl();
  filteredOptions: Observable<string[]> = of(this.options);

  ngOnInit() {
    this.filteredOptions = this.input.valueChanges.pipe(
      startWith(''),
      tap(value => this.onChange(value)),
      map(value => this._filter(value))
    );
  }

  writeValue(value: string): void {
    if (value) {
      this.input.patchValue(value);
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}
