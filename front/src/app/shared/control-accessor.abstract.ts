import { ControlValueAccessor } from '@angular/forms';

export abstract class AControlAccessor<T> implements ControlValueAccessor {
	value: T | undefined;
	
	/** method to call to update the model in the parent */
	setValue(val: T): void {
		if (this.value !== val) {
			this.value = val;
			this.onChange(val);
			this.onTouch(val);
		}
	}

	/* eslint-disable @typescript-eslint/no-empty-function*/
	/* eslint-disable @typescript-eslint/no-unused-vars*/
	onChange: (_: any) => void = (_) => {
	};
	onTouch: (_: any) => void = (_) => {
	};
	/* eslint-enable @typescript-eslint/no-empty-function */
	/* eslint-enable @typescript-eslint/no-unused-vars*/

	/** method called by the parent when it updates its model */
	writeValue(value: T): void {
		this.value = value;
		this.render();
	}

	registerOnChange(fn: () => void): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: () => void): void {
		this.onTouch = fn;
	}
	
	render(): void {
		return;
	}
}
