import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdiblesListEntryComponent } from './edibles-list-entry.component';

describe('EdiblesListEntryComponent', () => {
  let component: EdiblesListEntryComponent;
  let fixture: ComponentFixture<EdiblesListEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdiblesListEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdiblesListEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
