import { Component, Input, OnInit } from '@angular/core';
import { IEdible } from '@models';

@Component({
  selector: 'app-edibles-list-entry',
  templateUrl: './edibles-list-entry.component.html',
  styleUrls: ['./edibles-list-entry.component.scss']
})
export class EdiblesListEntryComponent implements OnInit {
  @Input() edible!: IEdible;

  constructor() { }

  ngOnInit(): void {
  }

}
