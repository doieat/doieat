import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdibleFeaturedComponent } from './edible-featured.component';

describe('EdibleFeaturedComponent', () => {
  let component: EdibleFeaturedComponent;
  let fixture: ComponentFixture<EdibleFeaturedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdibleFeaturedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdibleFeaturedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
