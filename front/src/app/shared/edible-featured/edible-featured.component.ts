import { Component, Input, OnInit } from '@angular/core';
import { Edible, IEdible } from '@models';

@Component({
  selector: 'app-edible-featured',
  templateUrl: './edible-featured.component.html',
  styleUrls: ['./edible-featured.component.scss']
})
export class EdibleFeaturedComponent implements OnInit {
  @Input() edible: IEdible = new Edible();

  constructor() { }

  ngOnInit(): void {
  }

}
