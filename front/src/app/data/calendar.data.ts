import { EdibleName } from '../models/edible-name.type';

/**
 * Indexes describe the availibity month of the listed edibles
 */
export const calendar: EdibleName[][] = [
	[ // JANUARY
		'Betterave',
		'Carotte',
		'Céleri',
		'Champignon de Paris',
		'Chou',
		'Chou de Bruxelles',
		'Chou-fleur',
		'Courge',
		'Cresson',
		'Endive',
		'Épinard',
		'Mâche',
		'Navet',
		'Oignon',
		'Panais',
		'Poireau',
		'Potiron',
		'Salsifis',
		'Topinambour',
		'Citron',
		'Clémentine',
		'Kiwi',
		'Mandarine',
		'Orange',
		'Poire',
		'Pomme',
	],
	[ // FEBRUARY
		'Betterave',
		'Carotte',
		'Céleri',
		'Champignon de Paris',
		'Chou',
		'Chou de Bruxelles',
		'Chou-fleur',
		'Cresson',
		'Endive',
		'Épinard',
		'Mâche',
		'Navet',
		'Oignon',
		'Panais',
		'Poireau',
		'Salsifis',
		'Topinambour',
		'Citron',
		'Clémentine',
		'Kiwi',
		'Mandarine',
		'Orange',
		'Pamplemousse',
		'Poire',
		'Pomme',
	],
	[ // MARCH
		'Betterave',
		'Carotte',
		'Céleri',
		'Champignon de Paris',
		'Chou',
		'Chou de Bruxelles',
		'Chou-fleur',
		'Cresson',
		'Endive',
		'Épinard',
		'Navet',
		'Oignon',
		'Panais',
		'Poireau',
		'Radis',
		'Kiwi',
		'Orange',
		'Pamplemousse',
		'Poire',
		'Pomme',
	],
	[ // AVRIL
		'Asperge',
		'Champignon de Paris',
		'Cresson',
		'Endive',
		'Épinard',
		'Fenouil',
		'Navet',
		'Oignon',
		'Poireau',
		'Radis',
		'Pamplemousse',
		'Pomme',
		'Rhubarbe',
	],
	[ // MAI
		'Artichaut',
		'Asperge',
		'Champignon de Paris',
		'Concombre',
		'Courgette',
		'Cresson',
		'Endive',
		'Épinard',
		'Laitue',
		'Navet',
		'Petit pois',
		'Radis',
		'Fraise',
		'Pamplemousse',
		'Rhubarbe',
	],
	[ // JUIN
		'Artichaut',
		'Asperge',
		'Aubergine',
		'Blette',
		'Champignon de Paris',
		'Concombre',
		'Courgette',
		'Fenouil',
		'Haricot vert',
		'Laitue',
		'Petit pois',
		'Radis',
		'Tomate',
		'Abricot',
		'Cassis',
		'Cerise',
		'Fraise',
		'Framboise',
		'Groseille',
		'Melon',
		'Pamplemousse',
		'Rhubarbe',
	],
	[ // JUILLET
		'Ail',
		'Artichaut',
		'Asperge',
		'Aubergine',
		'Blette',
		'Champignon de Paris',
		'Concombre',
		'Courgette',
		'Fenouil',
		'Haricot vert',
		'Laitue',
		'Maïs',
		'Petit pois',
		'Poivron',
		'Tomate',
		'Abricot',
		'Cassis',
		'Cerise',
		'Figue',
		'Fraise',
		'Framboise',
		'Groseille',
		'Melon',
		'Myrtille',
		'Nectarine',
		'Prune',
	],
	[ // AOÛT
		'Ail',
		'Artichaut',
		'Asperge',
		'Aubergine',
		'Blette',
		'Champignon de Paris',
		'Concombre',
		'Courgette',
		'Fenouil',
		'Haricot vert',
		'Laitue',
		'Maïs',
		'Poivron',
		'Tomate',
		'Abricot',
		'Cassis',
		'Figue',
		'Framboise',
		'Groseille',
		'Melon',
		'Mirabelle',
		'Mûre',
		'Myrtille',
		'Nectarine',
		'Poire',
		'Pomme',
		'Prune',
		'Quetsche',
	],
	[ // SEPTEMBRE
		'Ail',
		'Artichaut',
		'Asperge',
		'Aubergine',
		'Blette',
		'Brocoli',
		'Carotte',
		'Chou-fleur',
		'Champignon de Paris',
		'Concombre',
		'Courge',
		'Courgette',
		'Cresson',
		'Épinard',
		'Fenouil',
		'Haricot vert',
		'Laitue',
		'Maïs',
		'Oignon',
		'Poireau',
		'Poivron',
		'Potiron',
		'Tomate',
		'Figue',
		'Melon',
		'Mirabelle',
		'Mûre',
		'Myrtille',
		'Noisette',
		'Noix',
		'Poire',
		'Pomme',
		'Prune',
		'Quetsche',
		'Raisin',
		'Reine Claude',
	],
	[ // OCTOBRE
		'Ail',
		'Betterave',
		'Blette',
		'Brocoli',
		'Carotte',
		'Céleri',
		'Champignon de Paris',
		'Chou',
		'Chou de Bruxelles',
		'Chou-fleur',
		'Concombre',
		'Courge',
		'Courgette',
		'Cresson',
		'Échalote',
		'Endive',
		'Épinard',
		'Fenouil',
		'Haricot vert',
		'Mâche',
		'Navet',
		'Oignon',
		'Panais',
		'Poireau',
		'Poivron',
		'Potiron',
		'Châtaigne',
		'Coing',
		'Figue',
		'Noisette',
		'Noix',
		'Poire',
		'Pomme',
		'Quetsche',
		'Raisin',
	],
	[ // NOVEMBRE
		'Ail',
		'Betterave',
		'Brocoli',
		'Carotte',
		'Céleri',
		'Champignon de Paris',
		'Chou',
		'Chou de Bruxelles',
		'Chou-fleur',
		'Courge',
		'Cresson',
		'Échalote',
		'Endive',
		'Épinard',
		'Fenouil',
		'Mâche',
		'Navet',
		'Oignon',
		'Panais',
		'Poireau',
		'Potiron',
		'Salsifis',
		'Topinambour',
		'Châtaigne',
		'Citron',
		'Clémentine',
		'Kiwi',
		'Mandarine',
		'Noisette',
		'Poire',
		'Pomme',
	],
	[ // DÉCEMBRE
		'Ail',
		'Betterave',
		'Carotte',
		'Céleri',
		'Champignon de Paris',
		'Chou',
		'Chou de Bruxelles',
		'Chou-fleur',
		'Courge',
		'Cresson',
		'Échalote',
		'Endive',
		'Épinard',
		'Mâche',
		'Navet',
		'Oignon',
		'Panais',
		'Poireau',
		'Potiron',
		'Salsifis',
		'Topinambour',
		'Citron',
		'Clémentine',
		'Kiwi',
		'Mandarine',
		'Orange',
		'Poire',
		'Pomme',
	]
];

export function getEdibleAvailability(clue: string): number[] {
	return calendar.reduce((availabilityArray: number[], monthEdibles: string[], index: number) => {
		const hasEdible = !!monthEdibles.find(name => clue.includes(name));
		if (hasEdible) {
			availabilityArray.push(index);
		}
		return availabilityArray;
	}, []);
}

// Source: ADEME
// https://www.ademe.fr/sites/default/files/assets/documents/calendrier-fruits-legumes-de-saison.pdf