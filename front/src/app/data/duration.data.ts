export const day = 86400000;
export const week = 604800000;
export const month = 30.4167 * day;
export const year = 31536000000;