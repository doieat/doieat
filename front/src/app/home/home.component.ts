import { Component } from '@angular/core';
import * as dayjs from 'dayjs';

import { calendar } from '@data';
import { Edible } from '@models';
import { LoremIpsum } from 'lorem-ipsum';

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    min: 1,
    max: 3
  },
  wordsPerSentence: {
    min: 5,
    max: 10,
  }
})
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  currentMonth = dayjs().format('MMMM');
  featuredEdibles = calendar[dayjs().month()].map(edible => new Edible({ name: edible, description: lorem.generateParagraphs(1)}));
}
