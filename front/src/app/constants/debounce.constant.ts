export const DEBOUNCE_REGULAR = 250;
export const DEBOUNCE_SLOW = 500;
export const DEBOUNCE_FAST = 100;