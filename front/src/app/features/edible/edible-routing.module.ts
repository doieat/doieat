import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EdibleSearchComponent } from './edible-search/edible-search.component';

import { EdibleViewResolver } from './edible-view.resolver';
import { EdibleViewComponent } from './edible-view/edible-view.component';

const routes: Routes = [
	{ path: ':edibleId', component: EdibleViewComponent, resolve: { edible: EdibleViewResolver } },
	{ path: 'search', component: EdibleSearchComponent },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class EdibleRoutingModule {}