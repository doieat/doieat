import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdibleSearchComponent } from './edible-search.component';

describe('EdibleSearchComponent', () => {
  let component: EdibleSearchComponent;
  let fixture: ComponentFixture<EdibleSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdibleSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdibleSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
