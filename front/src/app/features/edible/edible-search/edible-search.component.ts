import { Component } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { debounceTime, filter, switchMap, tap } from 'rxjs/operators';

import { DEBOUNCE_REGULAR } from '@constants';
import { calendar } from '@data';
import { EdibleName, IEdible } from '@models';
import { EdibleInformationService } from '@shared';

@Component({
  selector: 'app-edible-search',
  templateUrl: './edible-search.component.html',
  styleUrls: ['./edible-search.component.scss']
})
export class EdibleSearchComponent {
  flat = (arr: any[]) => [].concat(...arr);
  ediblesNames: EdibleName[] = this.flat(calendar).filter((edible, index, self) => self.indexOf(edible) === index);
  selectedEdible: string = '';
  edibleClue: BehaviorSubject<string> = new BehaviorSubject('');
  isFirstSearch = true;

  ediblesList$: Observable<IEdible[]> = this.edibleClue
    .pipe(
      debounceTime(DEBOUNCE_REGULAR),
      filter((clue) => !!clue || this.isFirstSearch),
      tap((res) => this.isFirstSearch = false),
      switchMap((clue) => this.edibleInformationService.getEdibleInformation(clue)),
    );

  constructor(private edibleInformationService: EdibleInformationService) { }

  onResultSelected(result: string): void {
    this.edibleClue.next(result);
  }
}
