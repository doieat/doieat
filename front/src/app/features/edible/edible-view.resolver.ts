import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { IEdible } from '@models';
import { Observable } from 'rxjs';

import { EdibleInformationService } from '@shared';

@Injectable()
export class EdibleViewResolver implements Resolve<IEdible | undefined> {

	constructor(private edibleService: EdibleInformationService) {}

	resolve(route: ActivatedRouteSnapshot): Observable<IEdible | undefined> {
		const edibleId: string = route.paramMap.get('edibleId') || '';

		return this.edibleService.getEdibleById(+edibleId);
	}
}