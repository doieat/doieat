import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EdibleViewComponent } from './edible-view/edible-view.component';
import { EdibleRoutingModule } from './edible-routing.module';
import { EdibleViewResolver } from './edible-view.resolver';
import { EdibleSearchComponent } from './edible-search/edible-search.component';
import { SharedModule } from '@shared';

@NgModule({
  declarations: [
    EdibleViewComponent,
    EdibleSearchComponent,
  ],
  imports: [
    CommonModule,
    EdibleRoutingModule,
    FormsModule,
    SharedModule,
  ],
  providers: [
    EdibleViewResolver,
  ]
})
export class EdibleModule { }
