import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdibleViewComponent } from './edible-view.component';

describe('EdibleViewComponent', () => {
  let component: EdibleViewComponent;
  let fixture: ComponentFixture<EdibleViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdibleViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdibleViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
