import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IEdible } from '@models';

@Component({
  selector: 'app-edible-view',
  templateUrl: './edible-view.component.html',
  styleUrls: ['./edible-view.component.scss']
})
export class EdibleViewComponent implements OnInit {
  edible!: IEdible;

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.edible = this.route.snapshot.data.edible;
  }

}
