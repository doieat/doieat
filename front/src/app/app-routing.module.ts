import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { UtilComponent } from './util/util.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'edible', loadChildren: () => import('./features/edible').then(m => m.EdibleModule)},
  { path: 'util', component: UtilComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
